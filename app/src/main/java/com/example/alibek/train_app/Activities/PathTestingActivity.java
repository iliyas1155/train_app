package com.example.alibek.train_app.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.alibek.train_app.Cards.Path;
import com.example.alibek.train_app.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.example.alibek.train_app.Activities.PathsActivity.getChosenPathPosition;
import static com.example.alibek.train_app.Activities.PathsActivity.getPath;
import static com.example.alibek.train_app.Calculations.ListDataCalculator.calculatePressureInexes;

public class PathTestingActivity extends AppCompatActivity {
    LineChart chart;
    TextView pathName;
    TextView pathDesc;
    EditText massEditText;
    EditText resCoeffEditText;
    Button startPathTestButton;
    Path testingPath;
    int testingPathPosition;
    double mass = 0;
    double resCoeff = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_path_testing);
        chart = findViewById(R.id.path_test_chart);
        pathName = findViewById(R.id.testing_path_name);
        pathDesc = findViewById(R.id.testing_description);
        massEditText = findViewById(R.id.path_test_mass_edit_text);
        resCoeffEditText = findViewById(R.id.path_test_res_coeff_edit_text);
        startPathTestButton = findViewById(R.id.path_test_start_button);

        Log.d("PathTestingActivity","try to get Path");
        testingPathPosition = getChosenPathPosition();
        Log.d("PathTestingActivity","Path position = " + testingPathPosition);
        testingPath = getPath(testingPathPosition);

        pathName.setText(testingPath.name);
        pathDesc.setText("Above you can see graph of pressure index changing by position of path." + testingPath.vertical_data.size());
        Log.d("PathTestingActivity","testingPath.vertical_data = " + testingPath.vertical_data.size());
//        refreshChartData();

        startPathTestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mass = Double.parseDouble(massEditText.getText().toString());
                resCoeff = Double.parseDouble(resCoeffEditText.getText().toString());
                refreshChartData();
            }
        });

    }

    private void refreshChartData() {
        Log.d("PathTestingActivity","setNumericalData() start");
        ArrayList<Double> pathVerticalData = testingPath.vertical_data;
        Log.d("PathTestingActivity","setNumericalData() pathVerticalData = " + pathVerticalData.size());
        ArrayList<Double> pressureIndexes = calculatePressureInexes(mass, resCoeff, pathVerticalData);
        Log.d("PathTestingActivity","setNumericalData() pressureIndexes = " + pressureIndexes.size());
        float pos = 0f;
        List<Entry> entriesPath = new ArrayList();
        List<Entry> entriesVagon = new ArrayList();
        for(int i = 0; i<pressureIndexes.size(); i++){//300 because it's not able to show more+++++++++++++++
            entriesPath.add(new Entry(pos, pathVerticalData.get(i).floatValue()));
            entriesVagon.add(new Entry(pos, pressureIndexes.get(i).floatValue()));
            pos++;
        }
        LineDataSet linePathDataSet = new LineDataSet(entriesPath, "Path");
        LineDataSet lineVagonDataSet = new LineDataSet(entriesVagon, "Vagon");

        linePathDataSet.setColor(ColorTemplate.JOYFUL_COLORS[1]);
        linePathDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        linePathDataSet.setDrawCircles(false);
        linePathDataSet.setDrawCircleHole(false);

        lineVagonDataSet.setColor(ColorTemplate.LIBERTY_COLORS[1]);
        lineVagonDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineVagonDataSet.setDrawCircles(false);
        lineVagonDataSet.setDrawCircleHole(false);

        LineData lineData = new LineData();
        lineData.addDataSet(lineVagonDataSet);
        lineData.addDataSet(linePathDataSet);

        chart.setVisibleYRange(-10f, 100f, YAxis.AxisDependency.LEFT);
        chart.animateX(3000);
        chart.setData(lineData);
        chart.invalidate();

        Log.d("PathTestingActivity","setNumericalData() end ");
    }
}
