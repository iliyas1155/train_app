package com.example.alibek.train_app.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.EditText;

import com.example.alibek.train_app.R;

public class SequenseSettingActivity extends AppCompatActivity {
    EditText amountOfVagonsEditText;
    RecyclerView vagonsSettableRv, vagonsOptimalRv;
    Button setOptimalButton, testSequenseButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sequense_setting);

        amountOfVagonsEditText = findViewById(R.id.amount_of_vagons);
        vagonsSettableRv = findViewById(R.id.rv_vagons_settable);
        vagonsOptimalRv = findViewById(R.id.rv_vagons_optimal);
        setOptimalButton = findViewById(R.id.set_optimal_sequense);
        testSequenseButton = findViewById(R.id.test_sequense_button);

//        amountOfVagonsEditText.addTextChangedListener(new );
    }
}
