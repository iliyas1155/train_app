package com.example.alibek.train_app.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.alibek.train_app.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
